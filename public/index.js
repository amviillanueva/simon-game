const buttons = document.getElementsByClassName('btn');
const buttonColors = ["red", "blue", "green", "yellow"];
const levelTitle = document.getElementById('level-title');
const instruction = document.getElementById('instruction');
let sequence = [];
let userInput = [];
let start = true;
let level = 0;

Array.from(buttons).forEach((button) => {
    button.addEventListener('click', handleBtnClick);
});

document.addEventListener("keypress", (e) => {
    const key = e.key;
    if(key === 'Enter' && start === true) {
        levelTitle.textContent = `Level ${level + 1}`;
        instruction.textContent = 'Watch';
        addSequence();
        start = !start;
    }
    if(key === 'w') {
        handleBtnClick('green')
    }
    if(key === 'a') {
        handleBtnClick('red')
    }
    if(key === 's') {
        handleBtnClick('yellow')
    }
    if(key === 'd') {
        handleBtnClick('blue')
    }
});

function addSequence() {
    userInput = [];
    level ++;
    levelTitle.textContent = `Level ${level}`;
    instruction.textContent = 'Watch';

    const randomNumber = Math.floor(Math.random() * 4);
    const randomChosenColor = buttonColors[randomNumber];
    sequence.push(randomChosenColor);

    Array.from(sequence).forEach((color, index) => {
        setTimeout(() => {
            playSound(color);
            pressedAnimation(color);
        }, index * 1000);
    });

    setTimeout(() => {
        instruction.textContent = 'Play';
    }, sequence.length * 1000);
  }

function pressedAnimation(buttonId) {
    const buttonClicked = document.getElementById(buttonId);
    buttonClicked.className += ' pressed';
    setTimeout(() => {
        buttonClicked.classList.remove('pressed');
    }, 100);
}

function playSound(name) {
    const audio = new Audio('sounds/' + name + '.mp3');
    audio.play();
}

function checkAnswer(currentIndex) {
    if (sequence[currentIndex] === userInput[currentIndex]) {
        console.log("success");
        if (userInput.length === sequence.length){
            setTimeout(() => {
                addSequence(); 
            }, 1000)
        }
      } else {
        playSound('wrong');
        document.body.className += 'game-over';
        setTimeout(() => {
            document.body.classList.remove('game-over');
        }, 100)
        levelTitle.textContent = `Game Over`;
        instruction.textContent = 'Press enter to restart';
        start = !start;
        startOver();
      }
}

function handleBtnClick(e) {
    const color = e?.target?.id || e;
    playSound(color);
    pressedAnimation(color);

    userInput.push(color);
    checkAnswer(userInput.length-1);
}

function startOver() {
    sequence = [];
    userInput = [];
    start = true;
    level = 0;
}